class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates_length_of :email, :minimum => 5, :maximum => 250, :allow_blank => false
  validates_length_of :first_name, :minimum => 2, :maximum => 250, :allow_blank => false
  validates_length_of :last_name, :minimum => 2, :maximum => 250, :allow_blank => false
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
end
