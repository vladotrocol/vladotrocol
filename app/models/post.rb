class Post < ActiveRecord::Base
	acts_as_taggable_on :tags
	fuzzily_searchable :title
end
