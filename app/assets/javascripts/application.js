// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs

//= require bootstrap-sprockets

//= require moment
//= require bootstrap-datetimepicker
//= require parsley

//= require turbolinks

//= require_tree .

//= require plugins/block_styles.min.js
//= require plugins/colors.min.js
//= require plugins/media_manager.min.js
//= require plugins/tables.min.js
//= require plugins/video.min.js
//= require plugins/font_family.min.js
//= require plugins/font_size.min.js
//= require plugins/file_upload.min.js
//= require plugins/lists.min.js
//= require plugins/char_counter.min.js
//= require plugins/fullscreen.min.js
//= require plugins/urls.min.js
//= require plugins/inline_styles.min.js
//= require plugins/entities.min.js
//= require langs/ro.js

$(document).on('page:change page:restore', function(){
    var data = $('body').data();
    if (data.controller === 'posts' && (data.action === 'index'||data.action==='show' || data.action === 'tag')){
            $('pre code').each(function(i, e) {hljs.highlightBlock(e)});
    };
});
