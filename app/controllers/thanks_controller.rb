class ThanksController < ApplicationController
  before_action :set_thank, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  # GET /thanks
  # GET /thanks.json
  def index
    @thanks = Thank.page(params[:page]).per(5).order("created_at DESC")
  end

  # GET /thanks/1
  # GET /thanks/1.json
  def show
     @thanks = Thank.page(params[:page]).per(5).order("created_at DESC")
  end

  # GET /thanks/new
  def new
     @thanks = Thank.where(author: current_user.id).page(params[:page]).per(5).order("created_at DESC")
    @thank = Thank.new
  end

  # GET /thanks/1/edit
  def edit
  end

  # POST /thanks
  # POST /thanks.json
  def create
    @thank = Thank.new(thank_params)
    @thank.author = current_user.id

      if @thank.save
        redirect_to new_thank_path, notice: 'Thank was successfully created.' 
      else
        redirect_to new_thank_path, notice: 'Thank was not successfully created.' 
      end

  end

  # PATCH/PUT /thanks/1
  # PATCH/PUT /thanks/1.json
  def update
    respond_to do |format|
      if @thank.update(thank_params)
        format.html { redirect_to @thank, notice: 'Thank was successfully updated.' }
        format.json { render :show, status: :ok, location: @thank }
      else
        format.html { render :edit }
        format.json { render json: @thank.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /thanks/1
  # DELETE /thanks/1.json
  def delete
    @done = Thank.find(params[:id])
    if @done.destroy
       redirect_to new_thank_path, notice: 'Thank was successfully deleted.' 
    else
        redirect_to new_thank_path, notice: 'Thank was not successfully deleted.' 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_thank
      @thank = Thank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def thank_params
      params.require(:thank).permit(:content, :author)
    end
end
