class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  # GET /posts
  # GET /posts.json
  def index
    if params[:search]
      results = Post.find_by_fuzzy_title(params[:search])
      tagged_results = Post.tagged_with("#{params[:search]}", :any => true)
      @posts = (Kaminari.paginate_array((results+tagged_results).uniq)).page(params[:page]).per(1)
    else  
       @posts = Post.page(params[:page]).per(1).order("created_at DESC")  
    end
    @users = User.all
    @recent = Post.all.order("created_at desc").limit(5)
  end

  def tag
    @posts = Post.tagged_with(params[:tag]).page(params[:page]).per(2).order("created_at DESC")
    @users = User.all
    @recent = Post.all.order("created_at desc").limit(5)
    render :action => 'index'
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @users = User.all
    @recent = Post.all.order("created_at desc").limit(5)
  end

  # GET /posts/new
  def new
    @post = Post.new
    @recent = Post.all.order("created_at desc").limit(5)
  end

  # GET /posts/1/edit
  def edit
    @recent = Post.all.order("created_at desc").limit(5)
  end

  # POST /posts
  # POST /posts.json

  def create
    if current_user.admin>0
      @post = Post.new(post_params)
      @post.author = current_user.id
      respond_to do |format|
        if @post.save
          format.html { redirect_to @post, notice: 'Post was successfully created.' }
          format.json { render :show, status: :created, location: @post }
        else
          format.html { render :new }
          format.json { render json: @post.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to posts_path, notice: "Only admins can post new articles."
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    @recent = Post.all.order("created_at desc").limit(5)
    respond_to do |format|
      if current_user.admin>0
        if @post.update(post_params)
          format.html { redirect_to @post, notice: 'Post was successfully updated.' }
          format.json { render :show, status: :ok, location: @post }
        else
          format.html { render :edit }
          format.json { render json: @post.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
def delete
    @done = Post.find(params[:id])
    if current_user.admin>0
      if @done.destroy
        redirect_to posts_path, notice: "The article has been successfully deleted."
      else
        redirect_to posts_path, notice: "The article could not be deleted."
      end
    end
end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :description, :content, :author, :tag_list)
    end
end
