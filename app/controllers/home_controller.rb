class HomeController < ApplicationController
  require 'ExchangeRateVlad'
  ExchangeRate.init("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml")
  def index
  end
  def exchange
  	@currencies = ExchangeRate.get_currencies()
  	@times = ExchangeRate.get_times()
  end
  def convert
  	@currencies = ExchangeRate.get_currencies()
  	@times = ExchangeRate.get_times()

  	d = DateTime.parse(params[:home][:date])
    @r = [ExchangeRate.at(d, params[:home][:c1], params[:home][:c2]) * params[:home][:amount].to_f, 
    	params[:home][:c1], params[:home][:c2],params[:home][:amount], params[:home][:date]]
	p @r
   render :action => :exchange
  end
  def send_email
  	Emailer.raw_email('vladotrocol@gmail.com', params[:home][:subject], params[:home][:body]).deliver
  	redirect_to(:controller => '/home', :action => 'about')
  end
end
