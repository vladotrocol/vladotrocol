class ApplicationMailer < ActionMailer::Base
  default from: 'auto@vladotrocol.ro'
  layout 'mailer'
end
