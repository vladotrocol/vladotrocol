json.array!(@thanks) do |thank|
  json.extract! thank, :id
  json.url thank_url(thank, format: :json)
end
