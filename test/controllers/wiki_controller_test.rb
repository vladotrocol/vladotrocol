require 'test_helper'

class WikiControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get running-time" do
    get :running-time
    assert_response :success
  end

  test "should get linked-lists" do
    get :linked-lists
    assert_response :success
  end

end
