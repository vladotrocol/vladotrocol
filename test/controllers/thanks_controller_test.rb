require 'test_helper'

class ThanksControllerTest < ActionController::TestCase
  setup do
    @thank = thanks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:thanks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create thank" do
    assert_difference('Thank.count') do
      post :create, thank: {  }
    end

    assert_redirected_to thank_path(assigns(:thank))
  end

  test "should show thank" do
    get :show, id: @thank
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @thank
    assert_response :success
  end

  test "should update thank" do
    patch :update, id: @thank, thank: {  }
    assert_redirected_to thank_path(assigns(:thank))
  end

  test "should destroy thank" do
    assert_difference('Thank.count', -1) do
      delete :destroy, id: @thank
    end

    assert_redirected_to thanks_path
  end
end
