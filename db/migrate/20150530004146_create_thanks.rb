class CreateThanks < ActiveRecord::Migration
  def change
    create_table :thanks do |t|
	  t.string :content, null: false
      t.integer :author, null: false
      t.timestamps null: false
    end
  end
end
